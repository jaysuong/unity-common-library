﻿using System.Collections;
using NUnit.Framework;
using UnityEngine.TestTools;

namespace uCommonLib.Tests {
    public class ByteBoolTests {

        [Test]
        public void ByteBoolConversionTests () {
            var trueBoolRep = new ByteBool { Value = 1 };
            Assert.IsTrue ((ByteBool) trueBoolRep);

            var trueBool = true;
            Assert.IsTrue (((ByteBool) trueBool).Value > 0);
        }

        [Test]
        public void ByteBoolCreationEqualityTests () {
            Assert.IsTrue (ByteBool.CreateTrue ());
            Assert.IsFalse (ByteBool.CreateFalse ());

            var generatedTrue = ByteBool.CreateBool (true);
            var generatedFalse = ByteBool.CreateBool (false);
            Assert.IsTrue (generatedTrue);
            Assert.IsFalse (generatedFalse);
        }
    }
}