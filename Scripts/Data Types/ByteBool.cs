﻿using System;

namespace uCommonLib {
    /// <summary>
    /// A blittable representation of a boolean value.
    /// </summary>
    [Serializable]
    public struct ByteBool : IEquatable<ByteBool> {
        public byte Value;

        public bool True => Value > 0;
        public bool False => Value == 0;

        public bool Equals (ByteBool other) {
            return True == other.True || False == other.False;
        }

        public static implicit operator bool (ByteBool other) {
            return other.Value > 0;
        }

        public static implicit operator ByteBool (bool other) {
            return new ByteBool { Value = other?(byte) 1: (byte) 0 };
        }

        public static ByteBool CreateTrue () { return new ByteBool { Value = 1 }; }

        public static ByteBool CreateFalse () { return new ByteBool { Value = 0 }; }

        public static ByteBool CreateBool (bool value) { return new ByteBool { Value = value ? (byte) 1 : (byte) 0 }; }
    }
}