﻿using System.Collections.Generic;
using UnityEngine;

namespace uCommonLib.Generators {
    /// <summary>
    /// An on-demand object pooler that preallocates cloned instances as needed.
    /// Useful for high-performance object spawning lower garbage data loads than
    /// traditional Object.Instantiate
    /// </summary>
    [CreateAssetMenu (menuName = "Scriptable Objects/uCommonLib/Object Pool", fileName = "Object Pool")]
    public class ObjectPool : ScriptableObject {
        [SerializeField] private ushort poolSize = 10;

        public ushort PoolSize => poolSize;

        private Dictionary<int, Queue<GameObject>> pool;
        private Dictionary<int, int> spawnMap;

        private void OnEnable () {
            pool = new Dictionary<int, Queue<GameObject>> ();
            spawnMap = new Dictionary<int, int> ();
        }

        private void OnDisable () {
            Clear ();
        }

        /// <summary>
        /// Destroys all pooled instances from the game
        /// </summary>
        public void Clear () {
            foreach (var queue in pool.Values) {
                while (queue.Count > 0) {
                    Destroy (queue.Dequeue ());
                }
            }

            pool.Clear ();
            spawnMap.Clear ();
        }

        /// <summary>
        /// Places a spawned object back into the pool
        /// </summary>
        public void Despawn (GameObject obj) {
            int prefabID;

            if (spawnMap.TryGetValue (obj.GetInstanceID (), out prefabID)) {
                obj.SetActive (false);
                pool[prefabID].Enqueue (obj);
            }
        }

        /// <summary>
        /// Spawns a prefab clone from the pool. IF a pool did not exist before,
        /// then a new virtual pool is created
        /// </summary>
        public GameObject Spawn (GameObject prefab) {
            var prefabID = prefab.GetInstanceID ();
            Queue<GameObject> spawnQueue;

            if (!pool.TryGetValue (prefabID, out spawnQueue)) {
                spawnQueue = new Queue<GameObject> ();
                pool.Add (prefabID, spawnQueue);
            }

            if (spawnQueue.Count < 1) {
                ExpandQueue (ref spawnQueue, ref prefab, poolSize, ref spawnMap, prefabID);
            }

            var spawn = spawnQueue.Dequeue ();
            spawn.SetActive (true);
            return spawn;
        }

        private void ExpandQueue (ref Queue<GameObject> queue, ref GameObject prefab, ushort poolSize, ref Dictionary<int, int> map, int id) {
            for (ushort i = 0; i < poolSize; ++i) {
                var clone = Instantiate (prefab);
                clone.SetActive (false);
                queue.Enqueue (clone);

                map.Add (clone.GetInstanceID (), id);
            }
        }

    }
}